
INSERT INTO `listUserGroups` (`id`, `groupName`, `levels`) VALUES
	(1, 'Administration', 'Read Write'),
	(2, 'Administration', 'Read Only'),
	(3, 'Counsellor ', 'Read Only'),
	(4, 'Teachers', 'Read Write'),
	(5, 'Parents', 'Read Only');