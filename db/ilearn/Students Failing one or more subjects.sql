SELECT CONCAT_WS(' ',`stuFirstName`,`stuLastName`) AS 'Name', `Grade`.`graClsCode`,`graSubCode`,`graMid`
FROM `Grade`
INNER JOIN `Student` ON `Student`.`stuID` = `Grade`.`graStuID`
WHERE `graMid` < 60 and graTrmCode = 5
ORDER BY `graClsCode`, `graStuID`, `graSubCode`, CONCAT_WS(' ',`stuFirstName`,`stuLastName`)
