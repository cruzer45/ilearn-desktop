Menu
 Student
  Add Student
  Edit Student
  View Student
 Attendance
  Enter Attendance
  Edit Attendance
 Grades
  Record Grade
  Edit Grade
  Edit Report Card Remarks
  Remove Grades
 Merits
  Record Merits
  Edit Merits
 Demerits
  Record Demerits
  Edit Demerits
 Detention
  Add Detention
  Edit Detention
  Record Served Detention
 Reports
  Attendance Reports
   Attendance Summary
   Attendance Details
  Student Reports 
   Student List
   Repeating Students
   Students By Class
   Student ID Cards
  Class Reports
   Class List Report
   Class Grade Book
   Nationality Report
  Report Cards Menu
   Mid Term Reports
   Mid Term Class Ranking
   Term End Report
   Term End Ranking
  Demerit Reports
   Demerit Summary by Student
   Demerit Summary by Teacher
   Demerits By Class
   Demerits By Student
  Statistical Reports
   Class Size Distribution
   Gender Distribution
 Manage
  Class
   Add Class
   Edit Class
   View Class
  Promotions
   Assign Promotions
   Promote Students
  School
  Staff
   Add Staff
   Edit Staff
  Subjects
   Add Subjects 
   Edit Subjects
  Term
   Add Term
   Edit Term
  Time Slots
   Add Time Slot
   Edit Time Slot
  User Menu
   Add User
   Edit User
   Lock User
   Unlock User
  Utilities Menu
   Mid Term
    Calculate Mid Term Grades
    Reset Grade Remarks
    Clear Empty Grades
   End Of Term
    Calculate End of Term Grades
    Close Term