/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ilearn.kernel;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mrogers
 */
public class KeepAlive implements Runnable
{
    
    @Override
    public void run()
    {
        while (true)
        {
            try
            {
                PreparedStatement prep = Environment.getConnection().prepareStatement("SELECT 1;");
                ResultSet rs = prep.executeQuery();
                //Test if the connection is still alive
                if (rs.first()){
                    System.out.println("connection Alive");
                }
                //if it is not, recreate the connection
                else{
                    System.out.println("connection dead");
                    Environment.createConnection();
                }
                Thread.sleep(30000);
                
            }
            catch (InterruptedException ex)
            {
                Logger.getLogger(KeepAlive.class.getCanonicalName()).log(Level.SEVERE, "Failed to sleep keep alive thread", ex);
            }
            catch (SQLException ex)
            {
                Logger.getLogger(KeepAlive.class.getCanonicalName()).log(Level.SEVERE, "Cannot connect to database", ex);
            }
            
        }
    }
}
