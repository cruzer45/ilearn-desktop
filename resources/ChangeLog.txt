Version 1.10.1
-   Added GUI for the semester grade adjustment procedure.

Version 1.9.6
-   Changed the report card decimal places for the mid term report. the grades have no decimal places and the average have 2 decimal places.
-   Update to iReport 5.0.0

Version 1.9.3
-   Upgraded to Jasper Reports 4.7.1
-   Pushed the updating of GPA's to the database for faster processing.
-   Allowed the program to automatically clear and recalculate previously generated grades for the current semester.


Version 1.9.2
-   Fixed a bug that that prevented the weighted grades from being calculated with out of the entire 100% when it was not included.
-   Added consideration for absolute 0 grades when calculating weighting


Version 1.9.1
-   Updated to Jasper Reports 4.7.0

Version 1.9.0
    Included Student Transcripts

Version 1.7.9 For Georgetown
-   Set the Installer for georgetown to overwrite the previous config file.
-   Finished the student dashboard.
-   Finished the Teacher gradebook export.
-   Moved the export subject gradebook option to its own menu under reports.

Version 1.7.8 For Georgetown
-   Added the action taken field to the demerits module
-   Made the report card signature field picture editable


Version 1.6.0

-   Added the ability to remove grades from the system.

-   Fixed a bug that allowed a teacher to edit the grades of others.

-   Fixed a bug that prevented teachers from entering grades with a decimal point.

-   Added a query allowing you to view all grades for a specific subject.

-   Added Utilities to cleanup grades.

-   Added Delay to the dialog box so it can confirm before it generate grades.


Version 1.5.5

-   Update to iReport 4.1.2

-   Added Report displaying nationality status.

-   Added check to ensure that attendance would only be recorded once for a class for a day.

-   Added field in the student module to capture if they are Belizean or not.

Version 1.5.3 & 1.5.4

-   Attendance Form - Remove Demerit field, separate names (Last Name, First Name),place Tardy Field next to Absent.

-   Added Attendance summary report.

-   Added a demerit summary by teacher report.

-   Added a demerit summary by student report.

-   Added class size to the class list report.

-   Ordered Gradebook report by date.

-   Added sql query to pre-populate demerit reasons.

-   Fixed bug where you wouldn't be able to edit subject credits.


Version 1.5

-   Ordered Grade Book report by date

-   Added log and checker for one a term functions.

-   Added GPA function to the grade calculation.

-   Added close term sub functions.

-   Add Demerits and Term field to report cards.

-   Add Demerit Module (to be tied into detention module).

-   Add Detention Module (5 demerits = 1 Detention) Auto Detention.

-   Load Appropriate list items for classes and subjects.

-   Fix Add and Edit user screens to handle advanced permissions and linking.

-   Users will be linked to staff members.

-   H.O.D.s will be linked to all users they oversee.

-   Add Student ID Report.

-   Fixed error at login screen where IP array would return an error on Win XP
    machines with only an Ethernet Card.

-   Added Class Ranking Report.

-   Include Manage School menu option.

-   Add Simple class list report.

-   Add class Rank report.

-   Add title and assessment date to the Excel Grade Book export

-   Add Minimum, maximum and average functions to class grade book.

-   Add check to see if grade entered for student is more than the maximum
    points.

-   Include timeout field to "Edit User."

-   Added class size distribution report.

-   Added gender breakdown report.

-   Added demerit module.

-   Add Demerits Reports.

-   Add Student Status field to edit student.